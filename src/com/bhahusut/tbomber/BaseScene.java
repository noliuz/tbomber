package com.bhahusut.tbomber;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public abstract class BaseScene extends Scene {
	protected Engine eg;
    protected MainActivity act;
    protected VertexBufferObjectManager vbo;
    protected Camera cmr;
    public SceneManager.SceneType sceneType; 
    
    BaseScene(Engine _eg,MainActivity _act,VertexBufferObjectManager _vbo
    		,Camera _camera) {
    	eg = _eg;
    	act = _act;
    	vbo = _vbo;
    	cmr = _camera;
    	
    }
	
    public abstract void createScene();
    public abstract void loadGFX();
    public abstract void onBackKeyPressed();
    public abstract void disposeScene();
    
}
