package com.bhahusut.tbomber;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;

import android.view.KeyEvent;


public class MainActivity extends BaseGameActivity {

	final int mCameraWidth = 768;
	final int mCameraHeight = 1280;	  
	Engine eg;
	MainActivity act;
	VertexBufferObjectManager vbo;
	Camera cmr;
	
	PlayScene playScene;

    @Override
	public EngineOptions onCreateEngineOptions() {
		Camera mCamera = new Camera(0, 0, mCameraWidth, mCameraHeight);

		final EngineOptions engineOptions = new EngineOptions(true,
				ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(
						mCameraWidth, mCameraHeight), mCamera);
		engineOptions.getAudioOptions().setNeedsSound(true);
		engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
		return engineOptions;

	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		// TODO Auto-generated method stub
		pOnCreateResourcesCallback.onCreateResourcesFinished();

	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {
		// TODO Auto-generated method stub
		eg = mEngine;
		act = this;
		vbo = eg.getVertexBufferObjectManager();
		cmr = eg.getCamera();
		
		playScene = new PlayScene(eg,act,vbo,cmr);
		
		pOnCreateSceneCallback.onCreateSceneFinished(playScene);
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		// TODO Auto-generated method stub
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{  
	    if (keyCode == KeyEvent.KEYCODE_BACK)
	    {
	        System.exit(0);
	    }
	    return false; 
	}
}
