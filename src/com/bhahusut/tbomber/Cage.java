package com.bhahusut.tbomber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.AnimatedSprite.IAnimationListener;
import org.andengine.entity.sprite.Sprite;

import android.util.Log;

import com.bhahusut.tbomber.Figure.FigureSquare;

public class Cage {
	public static final int cageXBoxCount = 10;
	public static final int cageYBoxCount = 20;	
	public static final int boxW = 40;
	public static final int boxH = 40;	
	public static final int cageOffsetX = 190;
	public static final int cageOffsetY = 140;
	public static final int gravity =40;
	//public static final int gravity =5;
	
	int score=0;
	Figure figure;
	public int gTime = 700;
	gTimerTask gtTask;
	Timer timer;
	Cage cage;
	PlayScene sc;	
	ArrayList<CageGarbage> cgA = new ArrayList<CageGarbage>(); 
	boolean figureLanded = false;
	Line lineLeftCage,lineRightCage,lineBottomCage;
	ArrayList<CageGarbage> cgBigBombed = new ArrayList<CageGarbage>();
	ArrayList<BigBomb> bb80A = new ArrayList<BigBomb>();
	ArrayList<BigBomb> bb160A = new ArrayList<BigBomb>();
	ArrayList<CageGarbage> cgBufferA = new ArrayList<CageGarbage>();
	ArrayList<BigBomb> bb80BigBombed = new ArrayList<BigBomb>();
	ArrayList<BigBomb> bbBufferA = new ArrayList<BigBomb>();
	ArrayList<BombCoordinate> BombCoor40A = new ArrayList<BombCoordinate>();
	ArrayList<BombCoordinate> BombCoor80A = new ArrayList<BombCoordinate>();
	ArrayList<BombCoordinate> BombCoor160A = new ArrayList<BombCoordinate>();
	ArrayList<CageGarbage> removingCGA = new ArrayList<CageGarbage>();
	ArrayList<Coordinate> smallBombFireQueue = new ArrayList<Coordinate>(); 
	ArrayList<Coordinate> Bomb80FireQueue = new ArrayList<Coordinate>();
	ArrayList<Coordinate> Bomb160FireQueue = new ArrayList<Coordinate>();
	
	Cage(PlayScene _sc) {		
		cage = this;
		sc = _sc;				
	}
	
	void startGravity(int delay) {
		if (gtTask != null)
			gtTask.cancel();
		if (timer != null)
			timer.cancel();
		gtTask = new gTimerTask();
		timer = new Timer();
		timer.schedule(gtTask,0,delay);
	}
	
	void stopGravity() {
		timer.cancel();
	}
	
	void setFigure(Figure f) {
		figure = f;
		f.setOffset((Cage.cageXBoxCount-1)/2*boxW+Cage.cageOffsetX,
				Cage.cageOffsetY);		
		
		//this.drawFigureSprite(f);
	}
	
	public boolean isFigureTouchRightBorder() {
		int rightFigureOffset = figure.offsetX+(figure.getMaxRightPos()+1)*this.boxW;
		
		if (rightFigureOffset >= this.getCageRightOffset())
			return true;
		
		return false;
	}
	
	public boolean isFigureTouchLeftBorder() {
		int leftFigureOffset = figure.offsetX;
		
		if (leftFigureOffset <= cage.cageOffsetX)
			return true;
		
		return false;
	}
	
	public boolean isFigureCollidAnyGarbage(Figure f) {
		for (FigureSquare fs : f.fsA) {
			Sprite sp = fs.sp;
			Rect r_sp = new Rect(
					(int)sp.getX(),(int)sp.getY(),
					(int)sp.getX()+boxW,(int)sp.getY()+boxH);			
			for (CageGarbage cg : cgA) {
				Rect r_cg = new Rect(
					(int)cg.sp.getX(),(int)cg.sp.getY(),
					(int)cg.sp.getX()+boxW,(int)cg.sp.getY()+boxH);							
				
				
				//Log.e("collid",r_sp.x1+" "+r_sp.x2+" "+r_sp.y1+" "+r_sp.y2);
				
				if (r_cg.isOverlap(r_sp)) {
					Log.e("xx","Overlap now!!");
					return true;
				}
			}
		}		
		return false;
	}
	
	public int getCageRightOffset() {
		return cage.cageXBoxCount*cage.boxW+cage.cageOffsetX;
	}
	
	public boolean isFigureLanded() {
		//land on garbage
		for (CageGarbage cg : cgA) {
			for (FigureSquare fs : figure.fsA) {
				Sprite sp = fs.sp;
				int garbageEdgeX = cg.offsetX+boxW;
				int garbageEdgeY = cg.offsetY;
				int spEdgeX = (int)sp.getX()+boxW;
				int spEdgeY = (int)sp.getY()+boxH;
				
				if (garbageEdgeX == spEdgeX && garbageEdgeY == spEdgeY)
					return true;
			}			
		}
		
		//bottom cage
		int cageBottomOffset = cageOffsetY+cageYBoxCount*boxH; 
		for (FigureSquare fs : figure.fsA) {
			Sprite sp = fs.sp;
			if (sp.getY()+boxH >= cageBottomOffset)
				return true;
		}
		
		return false;
	}

	public boolean isFigureLeftSideCollidGarbage(Figure f) {
		//left
		for (CageGarbage cg : cgA) {
			for (FigureSquare fs : f.fsA) {
				//left				
				Sprite sp = fs.sp;
				int garbageRightEdgeX = cg.offsetX+boxW;
				int garbageRightEdgeY = cg.offsetY;
				int spLeftEdgeX = (int)sp.getX();
				int spLeftEdgeY = (int)sp.getY();
				
				if (garbageRightEdgeX == spLeftEdgeX && 
						garbageRightEdgeY == spLeftEdgeY)
					return true;
			}			
		}
	
		return false;
	}
	
	public boolean isFigureOutCage(Figure f) {    	
		
		int cageLeftX = Cage.cageOffsetX;
    	int cageRightX = Cage.cageOffsetX+Cage.cageXBoxCount*Cage.boxW;
    	int cageBottomY = Cage.cageOffsetY+Cage.cageYBoxCount*Cage.boxH;
    	
		//left
		if (f.offsetX < cageLeftX)
			return true;
		//right
		int ox = f.offsetX+(f.getMaxRightPos()+1)*Cage.boxW;
		if  (ox > cageRightX)
			return true;
		//bottom
    	int oy = f.offsetY+(f.getMaxBottomPos()+1)*Cage.boxH+Cage.cageOffsetY;
    	if (oy > cageBottomY)
    		return true;
		/*
		for (int i=0;i<4;i++) {
			if (f.SpA.get(i).collidesWith(lineLeftCage) ||
				f.SpA.get(i).collidesWith(lineRightCage) ||	
				f.SpA.get(i).collidesWith(lineBottomCage) ) {
				return true;
			}
		}*/		
    	
    	return false;
    }
	
	public boolean isFigureRightSideCollidGarbage(Figure f) {
		//right
		for (CageGarbage cg : cgA) {
			for (FigureSquare fs : f.fsA) {
				Sprite sp = fs.sp;
				//Right				
				int garbageLeftEdgeX = cg.offsetX;
				int garbageLeftEdgeY = cg.offsetY;
				int spRightEdgeX = (int)sp.getX()+boxW;
				int spRightEdgeY = (int)sp.getY();
				
				if (garbageLeftEdgeX == spRightEdgeX && 
						garbageLeftEdgeY == spRightEdgeY)
					return true;
			}			
		}
	
		return false;
	}	
	
	public Figure genNewFigure() {
		int min = 1;
		int max = 7;
		Random r = new Random();
		int i1 = r.nextInt(max-min+1)+min;
		
		Figure f=null;
		switch (i1) {
			case 1 : f = new FigureI();
				break;			
			case 2 : f = new FigureT();
				break;
			case 3 : f = new FigureO();
				break;
			case 4 : f = new FigureL();
				break;
			case 5 : f = new FigureJ();
				break;				
			case 6 : f = new FigureS();
				break;
			case 7 : f = new FigureZ();
				break;
		}									
		
		//debug
		f = new FigureO();
		
		//random insert bomb
		for (FigureSquare fs : f.fsA) {
			//random
			min = 1;
			max = 5;
			r = new Random();
			int k = r.nextInt(max-min+1)+min;
			if (k%2 == 0) {
				fs.isBomb = true;
			} else {
				fs.isBomb = false;
			}
			
			//debug 
			fs.isBomb = true;
		}
		
		
		this.genFigureSprite(f);
		//this.drawFigureSprite(f);
		
		return f;
	}
	
	public void newFallingFigure() {
		//scale queue figure back to normal
		Figure ff = sc.qFigure.get(0);
		for (FigureSquare fs : ff.fsA) {			
			fs.sp.setScale(1);
		}
				
		this.setFigure(ff);
				
		//init new figure queue
		sc.qFigure.remove(0);
		sc.qFigure.add(this.genNewFigure());

		sc.updateQFigure();
		
	}
	
	public Figure dup4ChkRotate() {
		int gval = figure.getGridVal();
		Figure ftmp=null;
				
		switch (gval) {
			case 1 : ftmp = new FigureI(); break;
			case 2 : ftmp = new FigureT(); break;
			case 3 : ftmp = new FigureO(); break;
			case 4 : ftmp = new FigureL(); break;
			case 5 : ftmp = new FigureJ(); break;
			case 6 : ftmp = new FigureS(); break;
			case 7 : ftmp = new FigureZ(); break;										
		}		
		ftmp.curRotation = figure.curRotation;
		ftmp.offsetX = figure.offsetX;
		ftmp.offsetY = figure.offsetY;
		
		ftmp.fsA = new ArrayList<FigureSquare>();
		//sc.cboxTR.setTexturePosition(3*40, 40);
		
		for (int i=0;i<4;i++) {			
			FigureSquare fs1 = new Figure.FigureSquare(
					figure.fsA.get(i).sx,figure.fsA.get(i).sy,false);
			fs1.sp = new Sprite(ftmp.offsetX+fs1.sx*boxW,
					ftmp.offsetY+fs1.sy*boxH,
					sc.cboxTR,sc.vbo);
			ftmp.fsA.add(fs1);						
		}
		this.genFigureSprite(ftmp);
		
		return ftmp;
	}
	
	public void suckThingsDown(int ly,int count) {
		//garbage
		for (CageGarbage cg : cgA) {
			int iy = ((int)cg.sp.getY()-cage.cageOffsetY)/boxH; 
			if (iy < ly) {//sucking				
				int mx = (int)cg.sp.getX();
				int my = (boxH*count)+(int)cg.sp.getY();
				cg.sp.setPosition(mx,my);
				cg.offsetX = mx;
				cg.offsetY = my;				
			}
		}
		
		//bomb80
		for (BigBomb bb : bb80A) {
			int iy = ((bb.offsetY-cageOffsetY)/boxH+1);
			if (iy < ly) {
				int mx = bb.offsetX;
				int my = (boxH*count)+bb.offsetY;
				bb.sp.setPosition(mx, my);
				bb.offsetX = mx;
				bb.offsetY = my;
			}
		}
		
		//bomb160
		for (BigBomb bb : bb160A) {
			int iy = ((bb.offsetY-cageOffsetY)/boxH+1);
			if (iy < ly) {
				int mx = bb.offsetX;
				int my = (boxH*count)+bb.offsetY;
				bb.sp.setPosition(mx, my);
				bb.offsetX = mx;
				bb.offsetY = my;
			}
		}
	}
	
	public void suckToBottom() {					
		//garbage
		CageGarbage[][] cgAA = 
				new CageGarbage[this.cageYBoxCount][this.cageXBoxCount];
			//init
		for (int j=0;j<this.cageYBoxCount;j++)
			for (int i=0;i<this.cageXBoxCount;i++)
				cgAA[j][i] = null;
			//fill caAA
		for (CageGarbage cg : cgA) {
			int ix = ((int)cg.sp.getX()-this.cageOffsetX)/boxW;
			int iy = ((int)cg.sp.getY()-this.cageOffsetY)/boxH;
			
			cgAA[iy][ix] = cg;			
		}
		
		//run all garbages from bottom to top
		for (int j=this.cageYBoxCount-1;j>=0;j--) {
			for (int i=0;i<this.cageXBoxCount;i++) {
				
				if (cgAA[j][i] == null)
					continue;
				
				CageGarbage cg = cgAA[j][i];
				
				
				BigBomb bb80 = this.getBigBomb80FromPos(i, j);
				BigBomb bb160 = this.getBigBomb160FromPos(i, j);
				if (bb80 != null) {//suck bomb80 to land
					boolean landed = false;
					while (true) {
						//if land on any garbage
						for (CageGarbage cg1 : cgA) {										
							//filter out of column garbage
							if (cg1.offsetX != bb80.offsetX &&
									  cg1.offsetX != bb80.offsetX+boxW) 																	
								continue;
							
							int garbageEdgeX = cg1.offsetX+boxW;
							int garbageEdgeY = cg1.offsetY;
							int bombEdgeX1 = bb80.offsetX+boxW;
							int bombEdgeX2 = bb80.offsetX+boxW*2;
							int bombEdgeY = bb80.offsetY+boxH*2;	
																						
							if ( (garbageEdgeX == bombEdgeX1 ||
								  garbageEdgeX == bombEdgeX2) &&	
								  garbageEdgeY == bombEdgeY) {								
								landed = true; //stop sucking
								break;//stop compare
							}							
						}
						
						//if land on bottom cage
						int bombEdgeY = bb80.offsetY+boxH*2;
						int cageBottomOffsetY = this.cageOffsetY+
								this.cageYBoxCount*boxH;
						if (bombEdgeY >= cageBottomOffsetY) {
							landed = true; //stop sucking
							break;// stop compare
						}
					
						if (landed) //stop sucking
							break;
						
						//bomb80 sucking
							//garbages in bomb80
						for (int m=0;m<2;m++) {
							for (int n=0;n<2;n++) {
								CageGarbage cgtmp = 
										getCGFromOffset(bb80.offsetX+boxW*n,
										bb80.offsetY+boxH*m);
								int mx = (int)cgtmp.sp.getX();
								int my = boxH+(int)cgtmp.sp.getY();
								cgtmp.sp.setPosition(mx,my);
								cgtmp.offsetX = mx;
								cgtmp.offsetY = my;
							}
						}					
							//bomb80 body						
						int my = bb80.offsetY+boxH;
						bb80.offsetY = my;
						bb80.sp.setPosition(bb80.offsetX, my);												
					}										
				} else if (bb160 != null) {//suck bomb160 to land
					boolean landed = false;
					while (true) {
						//if land on any garbage
						for (CageGarbage cg1 : cgA) {										
							//filter out of column garbage
							if (cg1.offsetX != bb160.offsetX &&
									  cg1.offsetX != bb160.offsetX+boxW &&
									  cg1.offsetX != bb160.offsetX+boxW*2 &&
									  cg1.offsetX != bb160.offsetX+boxW*3)  																		 
								continue;
							
							int garbageEdgeX = cg1.offsetX+boxW;
							int garbageEdgeY = cg1.offsetY;
							int bombEdgeX1 = bb160.offsetX+boxW;
							int bombEdgeX2 = bb160.offsetX+boxW*2;
							int bombEdgeX3 = bb160.offsetX+boxW*3;
							int bombEdgeX4 = bb160.offsetX+boxW*4;
							int bombEdgeY = bb160.offsetY+boxH*4;	
																						
							if ( (garbageEdgeX == bombEdgeX1 ||
								  garbageEdgeX == bombEdgeX2 ||
								  garbageEdgeX == bombEdgeX3 ||
								  garbageEdgeX == bombEdgeX4) &&	
								  garbageEdgeY == bombEdgeY) {								
								landed = true; //stop sucking
								break;//stop compare
							}							
						}
						
						//if land on bottom cage
						int bombEdgeY = bb160.offsetY+boxH*3;
						int cageBottomOffsetY = this.cageOffsetY+
								this.cageYBoxCount*boxH;
						if (bombEdgeY >= cageBottomOffsetY) {
							landed = true; //stop sucking
							break;// stop compare
						}
					
						if (landed) //stop sucking
							break;
						
						//bomb160 sucking
							//garbages in bomb160
						for (int m=0;m<4;m++) {
							for (int n=0;n<4;n++) {
								CageGarbage cgtmp = 
										getCGFromOffset(bb160.offsetX+boxW*n,
										bb160.offsetY+boxH*m);
								int mx = (int)cgtmp.sp.getX();
								int my = boxH+(int)cgtmp.sp.getY();
								cgtmp.sp.setPosition(mx,my);
								cgtmp.offsetX = mx;
								cgtmp.offsetY = my;
							}
						}					
							//bomb160 body						
						int my = bb160.offsetY+boxH;
						bb160.offsetY = my;
						bb160.sp.setPosition(bb160.offsetX, my);						
					}						
				} else {//suck garbage until land
					boolean landed = false;
					while (true) {
						//if land on any garbage
						for (CageGarbage cg1 : cgA) {
							//filter own garbage check
							if (cg1.offsetX == cg.offsetX && 
									cg1.offsetY == cg.offsetY)
								continue;
							
							//filter out of column garbage
							if (cg1.offsetX != cg.offsetX)
								continue;
																		
							int garbageEdgeX = cg1.offsetX+boxW;
							int garbageEdgeY = cg1.offsetY;
							int spEdgeX = (int)cg.sp.getX()+boxW;
							int spEdgeY = (int)cg.sp.getY()+boxH;	
																						
							if (garbageEdgeX == spEdgeX 
									&& garbageEdgeY == spEdgeY) {								
								landed = true; //stop suck
								break;//stop compare
							}							
						}
						
						//if land on bottom cage
						int garbageEdgeY = cg.offsetY;
						int cageBottomOffsetY = this.cageOffsetY+
								this.cageYBoxCount*boxH;
						if (garbageEdgeY+boxH >= cageBottomOffsetY) {
							landed = true;
							break;
						}
					
						if (landed) //stop sucking
							break;
						
						//garbage sucking
						int mx = (int)cg.sp.getX();
						int my = boxH+(int)cg.sp.getY();
						cg.sp.setPosition(mx,my);
						cg.offsetX = mx;
						cg.offsetY = my;
					}																								
				}
						
				
				
				/*
				while (true) {//loop until landed		
					//is landed? 
					boolean landed = false;
					for (CageGarbage cg1 : cgA) {//if landed on any garbage
						//chk for own garbage check
						if (cg1.offsetX == cg.offsetX && 
								cg1.offsetY == cg.offsetY)
							continue;																								
						
						//chk garbages of bomb80							
						int cgPosX = (int)(cg.offsetX-cageOffsetX)/boxW;
						int cgPosY = (int)(cg.offsetY-cageOffsetY)/boxH;																
						
						BigBomb bb80 = getBigBomb80FromPos(cgPosX,cgPosY);
						if (bb80 != null) {
							int bbEdgeX1 = bb80.offsetX+boxW;
							int bbEdgeX2 = bb80.offsetX+boxW;
							int bbEdgeY = bb80.offsetY+boxH;
							int cgSpEdgeX = cg1.offsetX+boxW;
							int cgSpEdgeY = cg1.offsetY;
							
							if ((cgSpEdgeX == bbEdgeX1 ||
								cgSpEdgeX == bbEdgeX2) &&
								cgSpEdgeY == bbEdgeY) {
								
								landed = true;
								break;
							}
						}
														
					//land on cage bottom
					int garbageEdgeY = cg.offsetY;
					int cageBottomOffsetY = this.cageOffsetY+
							this.cageYBoxCount*boxH;
					if (garbageEdgeY+boxH >= cageBottomOffsetY)
						landed = true;				
					if (landed)
						break;				
					
					//sucking
						//bomb80
					
					int posx = (int)(cg.sp.getX()-cageOffsetX)/boxW;
					int posy = (int)(cg.sp.getY()-cageOffsetY)/boxH;
					BigBomb bb = this.getBigBomb80FromTopLeftPos(posx, posy);
					if (bb != null) {
						bb.offsetY += boxH;
						bb.sp.setPosition(bb.offsetX,bb.offsetY);
					}
						//bomb160
					int posx1 = (int)(cg.sp.getX()-cageOffsetX)/boxW;
					int posy1 = (int)(cg.sp.getY()-cageOffsetY)/boxH;
					BigBomb bb1 = this.getBigBomb160FromTopLeftPos(posx, posy);
					if (bb1 != null) {
						bb1.offsetY += boxH;
						bb1.sp.setPosition(bb1.offsetX,bb1.offsetY);
					}					
						//garbage				
					
															
				}*/			
			}
		}
		
		this.clearFullLineGarbage();
	}
	
	public CageGarbage getCGFromOffset(int x,int y) {
		for (CageGarbage cg : cgA) {
			if (cg.offsetX == x && cg.offsetY == y)
				return cg;
		}
		
		return null;
	}
	
	public void genFigureSprite(Figure f) {				
		int k = f.getGridVal();
		if (k <= 4) {
			sc.cboxTR.setTexturePosition((k-1)*40,0);
		} else {
			k -= 4;
			sc.cboxTR.setTexturePosition((k-1)*40,40);
		}
						
		for (FigureSquare fs : f.fsA) {
			if (fs.sp != null) {				
				//debug
				this.detachSprite(fs.sp);
			}
			
			int fsx = f.offsetX+fs.sx*cage.boxW;
			int fsy = f.offsetY+fs.sy*cage.boxH;
				
			Sprite sp;
			if (fs.isBomb)
				sp = new Sprite(fsx,fsy,sc.bomb40TR,sc.vbo);
			else 
				sp = new Sprite(fsx,fsy,sc.cboxTR,sc.vbo);
						
			fs.sp = sp;			
		}
		
	}
	
	public void drawFigureSprite(Figure f) {
		for (FigureSquare fs : f.fsA) {			
			sc.attachChild(fs.sp);
		}
	}
	
	public void drawScaleFigureSprite(Figure f,float scale) {				
		for (FigureSquare fs : f.fsA) {						
			fs.sp.setScale(scale);
			fs.sp.setX(fs.sp.getX()*scale);
			fs.sp.setY(fs.sp.getY()*scale);		
			sc.attachChild(fs.sp);			
		}
	}
	
	public void drawCage() {
		/*
		//draw cage line for collid detection
		lineLeftCage = new Line(cageOffsetX-2,cageOffsetY,cageOffsetX-2,
				cageOffsetY+this.cageYBoxCount*boxH,sc.vbo);
		lineRightCage = new Line(cageOffsetX+cageXBoxCount*boxW,
				cageOffsetY,cageOffsetX+cageXBoxCount*boxW,
				cageOffsetY+this.cageYBoxCount*boxH,sc.vbo);
		lineBottomCage = new Line(cageOffsetX,cageOffsetY+cageYBoxCount*boxH,
				cageOffsetX+cageXBoxCount*boxW,
				cageOffsetY+cageYBoxCount*boxH,sc.vbo);
					
		lineLeftCage.setLineWidth(2);
		lineRightCage.setLineWidth(2);
		lineBottomCage.setLineWidth(2);
		lineLeftCage.setColor(255,255,255);
		lineRightCage.setColor(255,255,255);
		lineBottomCage.setColor(255,255,255);
		sc.attachChild(lineLeftCage);
		sc.attachChild(lineRightCage);
		sc.attachChild(lineBottomCage);
		*/				
		sc.cageSp = new Sprite(0,cageOffsetY-4,sc.cageTR,sc.vbo); 
		sc.attachChild(sc.cageSp);
	}
	
	class gTimerTask extends TimerTask {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			if (cage.isFigureLanded()) {
				cage.figureLanded = true;								
			}
			if (cage.figureLanded) {
				cage.figureLanded = false;
				cage.createGarbageFromFigure();
								
				cage.clearFullLineGarbage();		
				
				//clear figure
				for (FigureSquare fs : figure.fsA) {					
					detachSprite(fs.sp);
					fs.sp.dispose();
				}
				cage.figure.fsA.clear();															
				
				cage.newFallingFigure();				
				return;
			}
			
			figure.setOffset(figure.offsetX, figure.offsetY+gravity);			
			
		}	
	};
	
	public void createGarbageFromFigure() {
		sc.cboxTR.setTexturePosition(3*40, 40);
		for (FigureSquare fs : figure.fsA) {	
			Sprite sp = fs.sp;
			CageGarbage cg = new CageGarbage();
			cg.offsetX = (int)sp.getX();
			cg.offsetY = (int)sp.getY();
			
			if (fs.isBomb) {
				cg.sp = new Sprite(cg.offsetX,cg.offsetY,sc.bomb40TR,sc.vbo);
				cg.isBomb = true;
			} else {
				cg.sp = new Sprite(cg.offsetX,cg.offsetY,sc.cboxTR,sc.vbo);
				cg.isBomb = false;
			}
			
			cgA.add(cg);
			sc.attachChild(cg.sp);
												
		}
		
		//debug
		this.sumBomb80FromSmallBomb();
		this.sumBomb160FromBomb80();
	}
	
	public void addCageGarbage(int ox,int oy,boolean isBomb) {			
		CageGarbage cg = new CageGarbage();
		cg.offsetX = ox;
		cg.offsetY = oy;
		cg.isBomb = isBomb;
		if (cg.isBomb) {
			cg.sp = new Sprite(cg.offsetX,cg.offsetY,sc.bomb40TR,sc.vbo);			
		} else {
			cg.sp = new Sprite(cg.offsetX,cg.offsetY,sc.cboxTR,sc.vbo);			
		}
		
		cgA.add(cg);
		sc.attachChild(cg.sp);
	}
	
	public void sumBomb80FromSmallBomb() {		
		for (CageGarbage cg : cgA) {
			if (cg.isBomb && !this.isThisCGinCGA(cg, cgBigBombed)) {
				//check bomb 80x80
				// c mean check this position
				// x mean pinn position
				int countAroundBomb = 0;
				cgBufferA.clear();
				
				// cx
				// 00					
				int chkOffsetX = cg.offsetX-boxW;
				int chkOffsetY = cg.offsetY;					
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);
									
				// 0x
				// c0				
				chkOffsetX = cg.offsetX-boxW;
			    chkOffsetY = cg.offsetY+boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);						
			
				// 0x
				// 0c
				chkOffsetX = cg.offsetX;
				chkOffsetY = cg.offsetY+boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);				
								
				// add big bomb
				if (countAroundBomb == 3) {					
					//add Bigbomb80 array
					this.addBigBomb80(cg.offsetX-boxW, cg.offsetY);
					//add cg to checked array
					for (CageGarbage cg1 : cgBufferA) {
						cgBigBombed.add(cg1);
						//sc.detachSprite(cg1.sp);
					}			
					cgBigBombed.add(cg);
					//sc.detachSprite(cg.sp);
				}						
								
				// xc
				// 00
				countAroundBomb = 0;
				cgBufferA.clear();																	
				chkOffsetX = cg.offsetX+boxW;
				chkOffsetY = cg.offsetY;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);								
				// x0
				// 0c
				chkOffsetX = cg.offsetX+boxW;
				chkOffsetY = cg.offsetY+boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);			
				
				// x0
				// c0
				chkOffsetX = cg.offsetX;
				chkOffsetY = cg.offsetY+boxH;
				countAroundBomb +=
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);				
				// add big bomb
				if (countAroundBomb == 3) {
					//add Bigbomb80 array
					this.addBigBomb80(cg.offsetX, cg.offsetY);
					
					//add cg to checked array
					for (CageGarbage cg1 : cgBufferA) {
						cgBigBombed.add(cg1);																
					}									
					cgBigBombed.add(cg);
				}				
				
				// c0
				// x0				
				countAroundBomb = 0;
				cgBufferA.clear();
				
				chkOffsetX = cg.offsetX;
				chkOffsetY = cg.offsetY-boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);
				
				// 0c
				// x0								
				chkOffsetX = cg.offsetX+boxW;
				chkOffsetY = cg.offsetY-boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);			
				
				// 00
				// xc				
				chkOffsetX = cg.offsetX+boxW;
				chkOffsetY = cg.offsetY;
				countAroundBomb +=
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);				
				// add big bomb
				if (countAroundBomb == 3) {
					//add Bigbomb80 array
					this.addBigBomb80(cg.offsetX, cg.offsetY-boxH);
					//add cg to checked array
					for (CageGarbage cg1 : cgBufferA) {
						cgBigBombed.add(cg1);
					}									
					cgBigBombed.add(cg);
				}
			
				// 0c
				// 0x
				countAroundBomb = 0;
				cgBufferA.clear();								
				chkOffsetX = cg.offsetX;
				chkOffsetY = cg.offsetY-boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);				
				
				// c0
				// 0x				
				chkOffsetX = cg.offsetX-boxW;
				chkOffsetY = cg.offsetY-boxH;
				countAroundBomb += 
						chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);
				
				// 00
				// cx				
				chkOffsetX = cg.offsetX-boxW;
				chkOffsetY = cg.offsetY;
				countAroundBomb +=
					chkPosInSquareBomb(cgBufferA,chkOffsetX,chkOffsetY);
				
				// add big bomb
				if (countAroundBomb == 3) {
					//add Bigbomb80 array
					this.addBigBomb80(cg.offsetX-boxW, cg.offsetY-boxH);
					//add cg to checked array
					for (CageGarbage cg1 : cgBufferA) {
						cgBigBombed.add(cg1);
					}
					cgBigBombed.add(cg);
				}				
			}
		}			
	}
	
	public void sumBomb160FromBomb80() {		
		for (BigBomb bb : bb80A) {
			if (bb.isDisable)
				continue;
			//check bomb 160x160
			// c mean check this position
			// x mean pinn position
			int countAroundBomb = 0;
			bbBufferA.clear();																				
			// cx
			// 00					
			int chkOffsetX = bb.offsetX-boxW*2;
			int chkOffsetY = bb.offsetY;					
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);								
			// 0x
			// c0				
			chkOffsetX = bb.offsetX-boxW*2;
		    chkOffsetY = bb.offsetY+boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);								
			// 0x
			// 0c
			chkOffsetX = bb.offsetX;
			chkOffsetY = bb.offsetY+boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);				
							
			// add big bomb
			if (countAroundBomb == 3) {
				//add Bigbomb160 array
				this.addBigBomb160(bb.offsetX-boxW*2, bb.offsetY);
				//add bb to checked array
				for (BigBomb bb2 : bbBufferA) {
					bb2.isDisable = true;
					sc.detachSprite(bb2.sp);
				}			
				
				//remove 4 bomb80				
				sc.detachSprite(bb.sp);
				bb.isDisable = true;
			}						
								
			
			// xc
			// 00
			countAroundBomb = 0;
			bbBufferA.clear();																	
			chkOffsetX = bb.offsetX+boxW*2;
			chkOffsetY = bb.offsetY;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);								
			// x0
			// 0c
			chkOffsetX = bb.offsetX+boxW*2;
			chkOffsetY = bb.offsetY+boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);			
			
			// x0
			// c0
			chkOffsetX = bb.offsetX;
			chkOffsetY = bb.offsetY+boxH*2;
			countAroundBomb +=
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);				
			// add big bomb
			if (countAroundBomb == 3) {
				//add Bigbomb160 array
				this.addBigBomb160(bb.offsetX, bb.offsetY);
				//add bb to checked array
				for (BigBomb bb2 : bbBufferA) {
					bb2.isDisable = true;
					sc.detachSprite(bb2.sp);
				}			
				
				//remove 4 bomb80				
				sc.detachSprite(bb.sp);
				bb.isDisable = true;
			}				
			
			// c0
			// x0				
			countAroundBomb = 0;
			bbBufferA.clear();
			
			chkOffsetX = bb.offsetX;
			chkOffsetY = bb.offsetY-boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);
			
			// 0c
			// x0								
			chkOffsetX = bb.offsetX+boxW*2;
			chkOffsetY = bb.offsetY-boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);			
			
			// 00
			// xc				
			chkOffsetX = bb.offsetX+boxW*2;
			chkOffsetY = bb.offsetY;
			countAroundBomb +=
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);				
			// add big bomb
			if (countAroundBomb == 3) {
				//add Bigbomb160 array
				this.addBigBomb160(bb.offsetX, bb.offsetY-boxH*2);
				//add bb to checked array
				for (BigBomb bb2 : bbBufferA) {
					bb2.isDisable = true;
					sc.detachSprite(bb2.sp);
				}			
				
				//remove 4 bomb80				
				sc.detachSprite(bb.sp);
				bb.isDisable = true;
			}
		
			// 0c
			// 0x
			countAroundBomb = 0;
			cgBufferA.clear();								
			chkOffsetX = bb.offsetX;
			chkOffsetY = bb.offsetY-boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);				
			
			// c0
			// 0x				
			chkOffsetX = bb.offsetX-boxW*2;
			chkOffsetY = bb.offsetY-boxH*2;
			countAroundBomb += 
					chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);
			
			// 00
			// cx				
			chkOffsetX = bb.offsetX-boxW*2;
			chkOffsetY = bb.offsetY;
			countAroundBomb +=
				chkPosInSquareBigBomb80(bbBufferA,chkOffsetX,chkOffsetY);
			
			// add big bomb
			if (countAroundBomb == 3) {
				//add Bigbomb160 array
				this.addBigBomb160(bb.offsetX-boxW*2, bb.offsetY-boxH*2);
				//add bb to checked array
				for (BigBomb bb2 : bbBufferA) {
					bb2.isDisable = true;
					sc.detachSprite(bb2.sp);
				}			
				
				//remove 4 bomb80				
				sc.detachSprite(bb.sp);
				bb.isDisable = true;
			}								
		}
	}
	
	public void addBigBomb80(int ox,int oy) {
		//Log.e("addBigBomb80",ox+" "+oy);
		
		BigBomb bb = new BigBomb(ox,oy);
		Sprite sp = new Sprite(ox,oy,sc.bomb80TR,sc.vbo);
		bb.sp = sp;
		bb80A.add(bb);				
		sc.attachChild(sp);
		
		//clear sprite of small bomb			
		for (CageGarbage cg : cgA) {
			if (cg.offsetX == ox && cg.offsetY ==oy) {
				sc.detachSprite(cg.sp);
			}
				
			if (cg.offsetX == ox+boxW && cg.offsetY ==oy) {
				sc.detachSprite(cg.sp);
			}
			if (cg.offsetX == ox && cg.offsetY ==oy+boxH) {
				sc.detachSprite(cg.sp);
			}
			if (cg.offsetX == ox+boxW && cg.offsetY ==oy+boxH) {
				sc.detachSprite(cg.sp);
			}
		}		
				
	}	
	
	public void addBigBomb160(int ox,int oy) {
		//Log.e("addBigBomb80",ox+" "+oy);
		
		BigBomb bb = new BigBomb(ox,oy);
		Sprite sp = new Sprite(ox,oy,sc.bomb160TR,sc.vbo);
		bb.sp = sp;
		bb160A.add(bb);				
		sc.attachChild(sp);
		
		//clear sprite of small bomb
		for (BigBomb bb1 : this.bb80A) {
			if (bb1.offsetX == ox && bb1.offsetY ==oy)				
				sc.detachSprite(bb1.sp);
			if (bb1.offsetX == ox+boxW*2 && bb1.offsetY ==oy)
				sc.detachSprite(bb1.sp);
			if (bb1.offsetX == ox && bb1.offsetY ==oy+boxH*2)
				sc.detachSprite(bb1.sp);
			if (bb1.offsetX == ox+boxW*2 && bb1.offsetY ==oy+boxH*2)
				sc.detachSprite(bb1.sp);
		}
		
	}	
	
	public int chkPosInSquareBomb(ArrayList<CageGarbage> cgbuffA,int chkOffsetX,int chkOffsetY) {				
		if (isOffsetOutCage(chkOffsetX,chkOffsetY)) {		
			return 0;
		}
		
		//Log.e("x","8");
		CageGarbage cg1 = null;
		for (CageGarbage cg : cgA) {
			if (cg.offsetX == chkOffsetX && cg.offsetY == chkOffsetY) {
				cg1 = cg;
				break;
			}			
		}
		
		if (cg1 == null )
			return 0;		
				
		if (!cg1.isBomb)
			return 0;
				
		if (this.isThisCGinCGA(cg1, cgBigBombed))
			return 0;
		
		cgbuffA.add(cg1);
		return 1;						
	}

	public int chkPosInSquareBigBomb80(
		ArrayList<BigBomb> bbBuffA,int chkOffsetX,int chkOffsetY) {
		if (isOffsetOutCage(chkOffsetX,chkOffsetY)) {		

		}			
		
		BigBomb bb1 = null;
		for (BigBomb bb : this.bb80A) {
			if (bb.offsetX == chkOffsetX && bb.offsetY == chkOffsetY
					&& !bb.isDisable) {
				bb1 = bb;
				break;
			}			
		}
		
		if (bb1 == null )
			return 0;								
		
		bbBuffA.add(bb1);
		return 1;

	}

	
	public boolean isThereThisBigBomb(BigBomb bb,ArrayList<BigBomb> bbA) {
		for (BigBomb bb1 : bbA) {
			if (bb1.offsetX == bb.offsetX && bb1.offsetY == bb.offsetY)
				return true;
		}
		
		return false;
	}
	
	public void deleteThisBigBomb(BigBomb bb,ArrayList<BigBomb> bbA) {
		int i=0;
		for (BigBomb bb1 : bbA) {
			if (bb1.offsetX == bb.offsetX && bb1.offsetY == bb.offsetY)
				bbA.remove(i);
			i++;
		}
	}
	
	public boolean isThisCGinCGA(CageGarbage cg,ArrayList<CageGarbage> cgA1) {
		for (CageGarbage cg1 : cgA1) {
			if (cg1.offsetX == cg.offsetX && cg1.offsetY == cg.offsetY)
				return true;
		}
		
		return false;
	}
	
	public boolean isThisBBinBBA(BigBomb bb,ArrayList<BigBomb> bbA) {
		for (BigBomb bb1 : bbA) {
			if (bb1.offsetX == bb.offsetX && bb1.offsetY == bb.offsetY)
				return true;
		}
		
		return false;
	}
	
	public CageGarbage isThisGarbageBomb(int ox,int oy) {
		for (CageGarbage cg : cgA) {
			
			//if (cg.offsetX == ox && cg.offsetY == oy && cg.isBomb) ;
				//return cg;			
		}
		return null;
	}
	
	public BigBomb isThisBomb80(int ox,int oy) {
		for (BigBomb bb : bb80A) {
			if (bb.offsetX == ox && bb.offsetY == oy) 
				return bb;			
		}
		return null;
	}
	
	public boolean isOffsetOutCage(int ox,int oy) {
		if (ox >= cageOffsetX && 			
				ox <= cageOffsetX+boxW*this.cageXBoxCount &&
			oy >= cageOffsetY && 
			oy <= cageOffsetY+boxH*this.cageYBoxCount)
			return false;
		
		return true;
	}
	
	public void randomGenGarbage() {
		int min = 1;
		int max = Cage.cageYBoxCount-1;
		Random r = new Random();
		int iy = r.nextInt(max-min+1)+min;
		
		min = 0;
		max = Cage.cageXBoxCount-1;
		r = new Random();
		int ix = r.nextInt(max-min+1)+min;
		
		int ox = ix*boxW+cageOffsetX;
		int oy = iy*boxH+cageOffsetY;
		Sprite sp = new Sprite(ox,oy,sc.gboxTR,sc.vbo);
		sc.attachChild(sp);
		CageGarbage cg = new CageGarbage();
		cg.sp = sp;
		cg.offsetX = ox;
		cg.offsetY = oy;
		cgA.add(cg);
	}
	
	public void genGarbage(int ix,int iy) {			
		int ox = ix*boxW+cageOffsetX;
		int oy = iy*boxH+cageOffsetY;
		Sprite sp = new Sprite(ox,oy,sc.gboxTR,sc.vbo);
		sc.attachChild(sp);
		CageGarbage cg = new CageGarbage();
		cg.sp = sp;
		cg.offsetX = ox;
		cg.offsetY = oy;
		cgA.add(cg);
	}
	
	public void clearFullLineGarbage() {//if have a line, return true;
		//init Cage Garbage Array
		CageGarbage[][] garbageA = 
				new CageGarbage[this.cageYBoxCount][this.cageXBoxCount]; 				

		for (int i=0;i<cageYBoxCount;i++)
			for (int j=0;j<cageXBoxCount;j++)
				garbageA[i][j] = null;
		
		//fill Cage Garbage Array 
		for (CageGarbage cg : cgA) {
			int ix = ((int)cg.sp.getX()-this.cageOffsetX)/boxW;
			int iy = ((int)cg.sp.getY()-this.cageOffsetY)/boxH;
			
			garbageA[iy][ix] = cg;			
		}

		boolean haveLine = false;
		for (int j=0;j<cageYBoxCount;j++) {
			int count = 0;
			for (int i=0;i<cageXBoxCount;i++) {
				if (garbageA[j][i] != null)
					count++;
			}
			
			//found full line
			if (count == this.cageXBoxCount) {
				haveLine = true;
				//clear line
				ArrayList<Integer> iA = this.genRandomIndexClearLine();
				for (int i : iA) { 		
					if (garbageA[j][i] == null)
						continue;
					//trig small bomb
					if (garbageA[j][i].isBomb 
						&& !this.isThisCGinCGA(garbageA[j][i], cgBigBombed)) {
						this.trigSmallBomb(garbageA, i, j);
					}
					
					//remove garbages in line
					if (!this.isInCGBigBombA(garbageA[j][i])
							&& garbageA[j][i] != null) {
						detachSprite(garbageA[j][i].sp);
						removeCGAItem(garbageA[j][i]);															
						garbageA[j][i] = null;
						
						score++;
					}	
										
				}
				
				sc.updateScore(score);
				
				//trig bomb80
				Collections.shuffle(bb80A);
				for (BigBomb bb : this.bb80A) {
					if (bb.isDisable)
						continue;
					int oy = (bb.offsetY-this.cageOffsetY) / boxH;
					if (j == oy || j == oy+1) {
						this.trigBomb80(bb, garbageA);
					}
				}
				
				//trig bomb160
				for (BigBomb bb : this.bb160A) {
					if (bb.isDisable)
						continue;
										
					int oy = (bb.offsetY-this.cageOffsetY) / boxH;
					if (j == oy || j == oy+1 || j == oy+2 || j == oy+3) {
						this.trigBomb160(bb, garbageA);
					}										
				}								
								
			}						
		}	
				
		if (haveLine)
			suckToBottom();
	}
	
	public void trigBomb160(BigBomb bb,CageGarbage[][] cgtmpA) {
		int bx = this.cageOffsetX-40;
		int by = this.cageOffsetY+(800-530)/2;
		
		this.fireBomb160(bx, by);
		score += 20;		
		
		//clear garbage inside bomb160
		int ib = (bb.offsetX-this.cageOffsetX)/boxW;
		int jb = (bb.offsetY-this.cageOffsetY)/boxH;
		
		for (int k=0;k<4;k++) {
			for (int l=0;l<4;l++) {
				if (cgtmpA[jb+k][ib+l] != null) {
					this.removeCGBigBombAItem(cgtmpA[jb+k][ib+l]);
					removeCGAItem(cgtmpA[jb+k][ib+l]);								
					cgtmpA[jb+k][ib+l] = null;
				}
			}
		}
											
		//remove bomb160 sprite
		sc.detachSprite(bb.sp);
		bb.isDisable = true;
		
		//bomb radius
		for (Cage.BombCoordinate bc : this.BombCoor160A) {
			int i = (bb.offsetX - cageOffsetX)/boxW;
			int j = (bb.offsetY - cageOffsetY)/boxH;			
			
			int pos_i = i+bc.px;
			int pos_j = j+bc.py;
			
			if (pos_i >= this.cageXBoxCount ||
					pos_i < 0 ||
					pos_j >= this.cageYBoxCount ||
					pos_j < 0)
				continue;
			
			if (cgtmpA[pos_j][pos_i] == null)
				continue;
			
			//chk if this pos_i,pos_j is bomb
				//small bomb
			if (cgtmpA[pos_j][pos_i].isBomb &&
				!this.isThisCGinCGA(cgtmpA[pos_j][pos_i], cgBigBombed)) {
				this.trigSmallBomb(cgtmpA, pos_i, pos_j);
				

			} 
				//bomb80
			BigBomb pbb = this.getBigBomb80FromPos(pos_i, pos_j);
			if (pbb != null) {
				this.trigBomb80(pbb, cgtmpA);
				
			}
				//bomb160
			BigBomb pbb1 = this.getBigBomb160FromPos(pos_i, pos_j);
			if (pbb1 != null) {
				this.trigBomb160(pbb1, cgtmpA);
				
			}
			
			//are garbages so remove them
			if (cgtmpA[pos_j][pos_i] != null) {
				if (!this.isInCGBigBombA(cgtmpA[pos_j][pos_i])) {
						this.detachSprite(cgtmpA[pos_j][pos_i].sp);
						removeCGAItem(cgtmpA[pos_j][pos_i]);															
						cgtmpA[pos_j][pos_i] = null;
						
						score += 4;
				}
			}
		}
	}
	
	public void trigBomb80(BigBomb bb,CageGarbage[][] cgtmpA) {								
		this.fireBomb80(bb.offsetX-80, bb.offsetY-150);
		score += 10;
		
		//clear garbage inside bomb
		int ib = (bb.offsetX-this.cageOffsetX)/boxW;
		int jb = (bb.offsetY-this.cageOffsetY)/boxH;
		for (int k=0;k<2;k++) {
			for (int l=0;l<2;l++) {
				if (cgtmpA[jb+k][ib+l] != null) {
					removeCGBigBombAItem(cgtmpA[jb+k][ib+l]);
					removeCGAItem(cgtmpA[jb+k][ib+l]);
					
					cgtmpA[jb+k][ib+l] = null;
					
					score += 3;
				}
			}
		}					
		
		//remove bomb80 sprite
		sc.detachSprite(bb.sp);
		bb.isDisable = true;
		
		//bomb radius
		for (Cage.BombCoordinate bc : this.BombCoor80A) {
			int i = (bb.offsetX - cageOffsetX)/boxW;
			int j = (bb.offsetY - cageOffsetY)/boxH;			
			
			int pos_i = i+bc.px;
			int pos_j = j+bc.py;
			
			if (pos_i >= this.cageXBoxCount ||
					pos_i < 0 ||
					pos_j >= this.cageYBoxCount ||
					pos_j < 0)
				continue;
			
			if (cgtmpA[pos_j][pos_i] == null)
				continue;
			
			//chk if this pos_i,pos_j is bomb
				//small bomb
			if (cgtmpA[pos_j][pos_i].isBomb &&
				!this.isThisCGinCGA(cgtmpA[pos_j][pos_i], cgBigBombed)) {
				this.trigSmallBomb(cgtmpA, pos_i, pos_j);				
			} 
				//bomb80
			BigBomb pbb = this.getBigBomb80FromPos(pos_i, pos_j);
			if (pbb != null) {
				this.trigBomb80(pbb, cgtmpA);
				

			}
				//bomb160
			BigBomb pbb1 = this.getBigBomb160FromPos(pos_i, pos_j);
			if (pbb1 != null) {
				this.trigBomb160(pbb1, cgtmpA);
				

			}
			
			//are garbages so remove them
			if (cgtmpA[pos_j][pos_i] != null) {
				if (!this.isInCGBigBombA(cgtmpA[pos_j][pos_i])) {
						detachSprite(cgtmpA[pos_j][pos_i].sp);
						removeCGAItem(cgtmpA[pos_j][pos_i]);															
						cgtmpA[pos_j][pos_i] = null;
						
						score += 3;
				}
			}
		}
	}	
	
	public void trigSmallBomb(CageGarbage[][] cgtmpA ,int i,int j) {		
		int ox = cgtmpA[j][i].offsetX;
		int oy = cgtmpA[j][i].offsetY;
		
		this.fireSmallBomb(ox-71,oy-60);
		score += 5;
		
		//remove small bomb
		detachSprite(cgtmpA[j][i].sp);
		removeCGAItem(cgtmpA[j][i]);															
		cgtmpA[j][i] = null;
		
		//bomb radius
		for (Cage.BombCoordinate bc : this.BombCoor40A) {
			int pos_i = i+bc.px;
			int pos_j = j+bc.py;
			
			if (pos_i >= cageXBoxCount ||
				pos_i < 0 ||
				pos_j >= cageYBoxCount ||
				pos_j < 0) 				
					
				continue;
			
			if (cgtmpA[pos_j][pos_i] == null)
				continue;
			
			//chk if this pos_i,pos_j is bomb
				//small bomb
			if (cgtmpA[pos_j][pos_i].isBomb &&
					!this.isThisCGinCGA(cgtmpA[pos_j][pos_i], cgBigBombed)) {
				this.trigSmallBomb(cgtmpA, pos_i, pos_j);				
			} 					
				//bomb80
			BigBomb pbb = this.getBigBomb80FromPos(pos_i, pos_j);
			if (pbb != null) {
				this.trigBomb80(pbb, cgtmpA);							
			}
				//bomb160
			BigBomb pbb1 = this.getBigBomb160FromPos(pos_i, pos_j);
			if (pbb1 != null) {
				this.trigBomb160(pbb1, cgtmpA);								
			}
									
			//are garbages?, so remove them
			if (cgtmpA[pos_j][pos_i] != null) {
				if (!this.isInCGBigBombA(cgtmpA[pos_j][pos_i])) {
						detachSprite(cgtmpA[pos_j][pos_i].sp);
						removeCGAItem(cgtmpA[pos_j][pos_i]);															
						cgtmpA[pos_j][pos_i] = null;
				
						score += 2;
				}
			}
			
		} 
	}
				
	public BigBomb getBigBomb80FromPos(int px,int py) {
		for (BigBomb bb : bb80A) {
			if (bb.isDisable)
				continue;
			
			int bx = (bb.offsetX - cageOffsetX)/boxW;
			int by = (bb.offsetY - cageOffsetY)/boxH;
			
			if ( (px == bx && py == by) ||
				 (px == bx && py == by+1) ||
				 (px == bx+1 && py == by) ||
				 (px == bx+1 && py == by+1))
				
				return bb;
					
		}
		
		return null;
	}
	
	public BigBomb getBigBomb80FromTopLeftPos(int px,int py) {
		for (BigBomb bb : bb80A) {
			if (bb.isDisable)
				continue;
			
			int bx = (bb.offsetX - cageOffsetX)/boxW;
			int by = (bb.offsetY - cageOffsetY)/boxH;
			
			if (px == bx && py == by)				
				return bb;
					
		}
		
		return null;
	}
	
	public BigBomb getBigBomb160FromPos(int px,int py) {
		for (BigBomb bb : bb160A) {
			if (bb.isDisable)
				continue;
			
			int bx = (bb.offsetX - cageOffsetX)/boxW;
			int by = (bb.offsetY - cageOffsetY)/boxH;
			
			for (int j=0;j<4;j++) {
				for (int i=0;i<4;i++) {
					if (px == bx+i && py == by+j)
						return bb;
				}
			}					
		}
		
		return null;
	}
	
	public BigBomb getBigBomb160FromTopLeftPos(int px,int py) {
		for (BigBomb bb : bb160A) {
			if (bb.isDisable)
				continue;
			
			int bx = (bb.offsetX - cageOffsetX)/boxW;
			int by = (bb.offsetY - cageOffsetY)/boxH;
			
			
			if (px == bx && py == by)
				return bb;
								
		}
		
		return null;
	}
	
	public boolean isInCGBigBombA(CageGarbage cg) {
		if (cg == null)
			return false;
		
		for (CageGarbage cg1 : cgBigBombed) {
			if (cg1.offsetX == cg.offsetX && cg1.offsetY == cg.offsetY) {
				return true;
			}
		}
		
		return false;
	}
	
	public ArrayList<Integer> genRandomIndexClearLine() {
		ArrayList<Integer> iA = new ArrayList<Integer>();
		//init list
		for (int i=0;i<this.cageXBoxCount;i++)
			iA.add(i);
		
		//shuffle list
		Collections.shuffle(iA);
		
		return iA;
	}
	
	public void removeCGAItem(CageGarbage cg) {
		for (int i=0;i<cgA.size();i++) {
			CageGarbage cg1 = cgA.get(i);
			if (cg.offsetX == cg1.offsetX && cg.offsetY == cg1.offsetY) {
				cgA.remove(i);
			}
		}
	}
	
	public void removeCGBigBombAItem(CageGarbage cg) {
		for (int i=0;i<cgA.size();i++) {
			CageGarbage cg1 = this.cgBigBombed.get(i);
			if (cg.offsetX == cg1.offsetX && cg.offsetY == cg1.offsetY) {
				cgBigBombed.remove(i);
				break;
			}
		}
	}
	
	public void initSmallBombQueueHandler() {
		float waitSec = 0.05f;
		
		sc.registerUpdateHandler(new TimerHandler(waitSec, 
				true, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {         	
            	if (cage.smallBombFireQueue.size() == 0)
            		return; 
            	
            	Coordinate coor = cage.smallBombFireQueue.get(0);
            	cage.smallBombFireQueue.remove(0);
            	
            	AnimatedSprite aSp = 
        				new AnimatedSprite(coor.px,coor.py,
        						sc.explode40TTR,sc.vbo);
    			aSp.animate(140,0,new IAnimationListener() {

    				@Override
    				public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
    						int pInitialLoopCount) {
    					// TODO Auto-generated method stub
    					
    				}

    				@Override
    				public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
    						int pOldFrameIndex, int pNewFrameIndex) {
    					// TODO Auto-generated method stub
    					
    				}

    				@Override
    				public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
    						int pRemainingLoopCount, int pInitialLoopCount) {
    					// TODO Auto-generated method stub
    					
    				}

    				@Override
    				public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
    					// TODO Auto-generated method stub
    					sc.detachSprite(pAnimatedSprite);
    				}
    				
    			});
    			
    			sc.attachChild(aSp);

            }
        }));

	}

	public void initBomb80QueueHandler() {
		float waitSec = 0.1f;
		
		sc.registerUpdateHandler(new TimerHandler(waitSec, 
				true, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {         	
            	if (cage.Bomb80FireQueue.size() == 0)
            		return;
            	
            	Coordinate coor = cage.Bomb80FireQueue.get(0);
            	cage.Bomb80FireQueue.remove(0);
            	
            	AnimatedSprite aSp = 
            			new AnimatedSprite(coor.px,coor.py,sc.explode80TTR,sc.vbo);
            		aSp.animate(50,0,new IAnimationListener() {

            			@Override
            			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
            					int pInitialLoopCount) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
            					int pOldFrameIndex, int pNewFrameIndex) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
            					int pRemainingLoopCount, int pInitialLoopCount) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
            				// TODO Auto-generated method stub
            				sc.detachSprite(pAnimatedSprite);
            			}
            			
            		});
            		
            		sc.attachChild(aSp);
    			            	
            }
        }));

	}

	public void initBomb160QueueHandler() {
		float waitSec = 0.1f;
		
		sc.registerUpdateHandler(new TimerHandler(waitSec, 
				true, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {         	
            	if (cage.Bomb160FireQueue.size() == 0)
            		return;
            	
            	Coordinate coor = cage.Bomb160FireQueue.get(0);
            	cage.Bomb160FireQueue.remove(0);
            	
            	AnimatedSprite aSp = 
            			new AnimatedSprite(coor.px,coor.py,sc.explode160TTR,sc.vbo);
            		aSp.animate(/*200*/100,0,new IAnimationListener() {

            			@Override
            			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
            					int pInitialLoopCount) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
            					int pOldFrameIndex, int pNewFrameIndex) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
            					int pRemainingLoopCount, int pInitialLoopCount) {
            				// TODO Auto-generated method stub
            				
            			}

            			@Override
            			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
            				// TODO Auto-generated method stub
            				sc.detachSprite(pAnimatedSprite);
            			}
            			
            		});
            		
            		sc.attachChild(aSp);
    			            	
            }
        }));

	}

	
	public void fireSmallBomb(final int offsetX,final int offsetY) {
		smallBombFireQueue.add(new Coordinate(offsetX,offsetY));
		
		/*
		//debug
		if (cage.smallBombFireQueue.size() == 0)
    		return; 
    	
    	Coordinate coor = cage.smallBombFireQueue.get(0);
    	cage.smallBombFireQueue.remove(0);
    	
    	AnimatedSprite aSp = 
				new AnimatedSprite(coor.px,coor.py,
						sc.explode40TTR,sc.vbo);
		aSp.animate(140,0,new IAnimationListener() {

			@Override
			public void onAnimationStarted(AnimatedSprite pAnimatedSprite,
					int pInitialLoopCount) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite,
					int pOldFrameIndex, int pNewFrameIndex) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite,
					int pRemainingLoopCount, int pInitialLoopCount) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
				// TODO Auto-generated method stub
				sc.detachSprite(pAnimatedSprite);
			}
			
		});
		
		sc.attachChild(aSp);

			*/					
	}
	
	public void detachSprite(Sprite _sp) {
		final Sprite sp = _sp;
		sc.eg.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				sp.detachSelf();
			}						
		});
	}
	
	public void fireBomb80(int offsetX,int offsetY) {
		this.Bomb80FireQueue.add(new Coordinate(offsetX,offsetY));				 		
	}

	public void fireBomb160(int offsetX,int offsetY) {
		this.Bomb160FireQueue.add(new Coordinate(offsetX,offsetY));								
	}
	
	public void initBombCoordinate() {
		//Bomb40
		BombCoor40A.add(new BombCoordinate(0,-1));
		BombCoor40A.add(new BombCoordinate(-1,0));
		BombCoor40A.add(new BombCoordinate(1,0));
		BombCoor40A.add(new BombCoordinate(0,1));
		
		//Bomb80
		BombCoor80A.add(new BombCoordinate(-1,0));
		BombCoor80A.add(new BombCoordinate(-2,0));
		BombCoor80A.add(new BombCoordinate(2,0));
		BombCoor80A.add(new BombCoordinate(3,0));
		BombCoor80A.add(new BombCoordinate(-1,1));
		BombCoor80A.add(new BombCoordinate(-2,1));
		BombCoor80A.add(new BombCoordinate(2,1));
		BombCoor80A.add(new BombCoordinate(3,1));		
		BombCoor80A.add(new BombCoordinate(0,-1));
		BombCoor80A.add(new BombCoordinate(0,-2));
		BombCoor80A.add(new BombCoordinate(0,2));
		BombCoor80A.add(new BombCoordinate(0,3));		
		BombCoor80A.add(new BombCoordinate(1,-1));
		BombCoor80A.add(new BombCoordinate(1,-2));
		BombCoor80A.add(new BombCoordinate(1,2));
		BombCoor80A.add(new BombCoordinate(1,3));
		BombCoor80A.add(new BombCoordinate(-1,-1));
		BombCoor80A.add(new BombCoordinate(2,-1));
		BombCoor80A.add(new BombCoordinate(2,2));
		BombCoor80A.add(new BombCoordinate(-1,2));
		
		BombCoor160A.add(new BombCoordinate(1,-1));
		BombCoor160A.add(new BombCoordinate(1,-2));
		BombCoor160A.add(new BombCoordinate(1,-3));
		BombCoor160A.add(new BombCoordinate(1,-4));
		BombCoor160A.add(new BombCoordinate(1,4));
		BombCoor160A.add(new BombCoordinate(1,5));
		BombCoor160A.add(new BombCoordinate(1,6));
		BombCoor160A.add(new BombCoordinate(1,7));
		BombCoor160A.add(new BombCoordinate(1,-1));
		BombCoor160A.add(new BombCoordinate(1,-2));
		BombCoor160A.add(new BombCoordinate(1,-3));
		BombCoor160A.add(new BombCoordinate(1,-4));
		BombCoor160A.add(new BombCoordinate(1,4));
		BombCoor160A.add(new BombCoordinate(1,5));
		BombCoor160A.add(new BombCoordinate(1,6));
		BombCoor160A.add(new BombCoordinate(1,7));

		BombCoor160A.add(new BombCoordinate(2,-1));
		BombCoor160A.add(new BombCoordinate(2,-2));
		BombCoor160A.add(new BombCoordinate(2,-3));
		BombCoor160A.add(new BombCoordinate(2,-4));
		BombCoor160A.add(new BombCoordinate(2,4));
		BombCoor160A.add(new BombCoordinate(2,5));
		BombCoor160A.add(new BombCoordinate(2,6));
		BombCoor160A.add(new BombCoordinate(2,7));
		
		BombCoor160A.add(new BombCoordinate(3,-1));
		BombCoor160A.add(new BombCoordinate(3,-2));
		BombCoor160A.add(new BombCoordinate(3,-3));
		BombCoor160A.add(new BombCoordinate(3,-4));
		BombCoor160A.add(new BombCoordinate(3,4));
		BombCoor160A.add(new BombCoordinate(3,5));
		BombCoor160A.add(new BombCoordinate(3,6));
		BombCoor160A.add(new BombCoordinate(3,7));
		
		BombCoor160A.add(new BombCoordinate(4,0));
		BombCoor160A.add(new BombCoordinate(4,-1));
		BombCoor160A.add(new BombCoordinate(4,-2));
		BombCoor160A.add(new BombCoordinate(4,-3));
		BombCoor160A.add(new BombCoordinate(4,1));
		BombCoor160A.add(new BombCoordinate(4,2));
		BombCoor160A.add(new BombCoordinate(4,3));
		BombCoor160A.add(new BombCoordinate(4,4));
		BombCoor160A.add(new BombCoordinate(4,5));
		BombCoor160A.add(new BombCoordinate(4,6));
		
		BombCoor160A.add(new BombCoordinate(5,0));
		BombCoor160A.add(new BombCoordinate(5,-1));
		BombCoor160A.add(new BombCoordinate(5,-2));
		BombCoor160A.add(new BombCoordinate(5,-3));
		BombCoor160A.add(new BombCoordinate(5,1));
		BombCoor160A.add(new BombCoordinate(5,2));
		BombCoor160A.add(new BombCoordinate(5,3));
		BombCoor160A.add(new BombCoordinate(5,4));
		BombCoor160A.add(new BombCoordinate(5,5));
		BombCoor160A.add(new BombCoordinate(5,6));
		
		BombCoor160A.add(new BombCoordinate(6,0));
		BombCoor160A.add(new BombCoordinate(6,-1));
		BombCoor160A.add(new BombCoordinate(6,-2));
		BombCoor160A.add(new BombCoordinate(6,1));
		BombCoor160A.add(new BombCoordinate(6,2));
		BombCoor160A.add(new BombCoordinate(6,3));
		BombCoor160A.add(new BombCoordinate(6,4));
		BombCoor160A.add(new BombCoordinate(6,5));
		
		BombCoor160A.add(new BombCoordinate(7,0));
		BombCoor160A.add(new BombCoordinate(7,1));
		BombCoor160A.add(new BombCoordinate(7,2));
		BombCoor160A.add(new BombCoordinate(7,3));
		
		BombCoor160A.add(new BombCoordinate(-1,0));
		BombCoor160A.add(new BombCoordinate(-1,-1));
		BombCoor160A.add(new BombCoordinate(-1,-2));
		BombCoor160A.add(new BombCoordinate(-1,-3));
		BombCoor160A.add(new BombCoordinate(-1,1));
		BombCoor160A.add(new BombCoordinate(-1,2));
		BombCoor160A.add(new BombCoordinate(-1,3));
		BombCoor160A.add(new BombCoordinate(-1,4));
		BombCoor160A.add(new BombCoordinate(-1,5));
		BombCoor160A.add(new BombCoordinate(-1,6));
		
		BombCoor160A.add(new BombCoordinate(-2,0));
		BombCoor160A.add(new BombCoordinate(-2,-1));
		BombCoor160A.add(new BombCoordinate(-2,-2));
		BombCoor160A.add(new BombCoordinate(-2,-3));
		BombCoor160A.add(new BombCoordinate(-2,1));
		BombCoor160A.add(new BombCoordinate(-2,2));
		BombCoor160A.add(new BombCoordinate(-2,3));
		BombCoor160A.add(new BombCoordinate(-2,4));
		BombCoor160A.add(new BombCoordinate(-2,5));
		BombCoor160A.add(new BombCoordinate(-2,6));
		
		BombCoor160A.add(new BombCoordinate(-3,0));
		BombCoor160A.add(new BombCoordinate(-3,-1));
		BombCoor160A.add(new BombCoordinate(-3,-2));
		BombCoor160A.add(new BombCoordinate(-3,1));
		BombCoor160A.add(new BombCoordinate(-3,2));
		BombCoor160A.add(new BombCoordinate(-3,3));
		BombCoor160A.add(new BombCoordinate(-3,4));
		BombCoor160A.add(new BombCoordinate(-3,5));
		
		BombCoor160A.add(new BombCoordinate(-4,0));
		BombCoor160A.add(new BombCoordinate(-4,1));
		BombCoor160A.add(new BombCoordinate(-4,2));
		BombCoor160A.add(new BombCoordinate(-4,3));
	}
	
	class CageGarbage {
		int offsetX,offsetY;
		Sprite sp;
		boolean isBomb;		
	};
	
	public static class Rect {
		int x1,x2,y1,y2;
		
		Rect(int _x1,int _y1,int _x2,int _y2) {
			x1 = _x1;
			x2 = _x2;
			y1 = _y1;
			y2 = _y2;
		}
		
		boolean valueInRange(int value,int min,int max) {
			return (value >= min) && (value <= max);
		}
		
		public boolean isOverlap(Rect r2) {
			boolean xOverlap = valueInRange(x1,r2.x1,r2.x2) ||
							valueInRange(r2.x1,x1,x2);
					
			boolean yOverlap = valueInRange(y1,r2.y1,r2.y2) ||
							valueInRange(r2.y1,y1,y2);
			
			/*
			Log.e("overlap",
					x1+" "+y1+" "+x2+" "+y2+" "+xOverlap+
					r2.x1+" "+r2.y1+" "+r2.x2+" "+r2.y2+" "+yOverlap);
			*/
			
			return xOverlap && yOverlap;			
		}
	};
	
	public static class BigBomb {
		int offsetX;
		int offsetY;
		Sprite sp;
		boolean isDisable;
		
		BigBomb(int ox,int oy) {
			offsetX = ox;
			offsetY = oy;
			isDisable = false;
		}
	}
	
	public static class BombCoordinate {
		int px;
		int py;
		
		BombCoordinate(int _px,int _py) {
			px = _px;
			py = _py;
		}
	}		
	
	public static class Coordinate {
		int px;
		int py;
		
		Coordinate(int _px,int _py) {
			px = _px;
			py = _py;
		}
	}
}

