package com.bhahusut.tbomber;
import java.util.ArrayList;

import org.andengine.entity.sprite.Sprite;
import org.andengine.util.color.Color;

public abstract class Figure {
    
    protected final static int I = 1;
    protected final static int T = 2;
    protected final static int O = 3;
    protected final static int L = 4;
    protected final static int J = 5;
    protected final static int S = 6;
    protected final static int Z = 7;
    
    public int curRotation;  
        
    public ArrayList<Point> bombPointA = new ArrayList<Point>();
    
    protected int offsetX;
    protected int offsetY;
    
    protected int offsetXLast;
    protected int offsetYLast;
        
    public ArrayList< FigureSquare> fsA = new ArrayList<FigureSquare>();   
    
    protected Figure() {        
        offsetYLast = offsetY = 0;
        offsetXLast = offsetX = 4;
    }               
    
    public int getMaxRightOffset() {

    	return 0;
    }
    
    public int getMaxRightPos() {
    	int maxx=0;
    	for (FigureSquare fs : fsA) {
    		if (fs.sx > maxx)
    			maxx = fs.sx;
    	}
    	    	
    	return maxx;
    }
    
    public int getMaxBottomPos() {
    	
    	int maxy=0;
    	for (FigureSquare fs : fsA) {
    		if (fs.sy > maxy)
    			maxy = fs.sy;
    	}
    	    	
    	return maxy;
    	
    }        
    
    public void setOffset(int x, int y) {
        offsetXLast = offsetX;
        offsetYLast = offsetY; 
        offsetX = x;
        offsetY = y;      
        
        //set offset of sprite
        for (FigureSquare fs : this.fsA) {
        	int nOffsetX = (int)(offsetX+fs.sx*Cage.boxW*fs.sp.getScaleX());
        	int nOffsetY = (int)(offsetY+fs.sy*Cage.boxH*fs.sp.getScaleY());
        	
        	fs.sp.setX(nOffsetX);
        	fs.sp.setY(nOffsetY);
        }        
    }
           
    protected void resetOffsets() {
        offsetX = offsetY = offsetXLast = offsetYLast = 0;
    }            
    
    protected abstract void rotationRight();
    
    protected abstract void rotationLeft();
    
    protected abstract int getGridVal();    
    
    public class Point {
    	int x,y;
    	
    	Point(int _x,int _y) {
    		x = _x;
    		y = _y;
    	}
    }
    
    public static class FigureSquare {
    	int sx,sy; //square x,y
    	boolean isBomb;
    	Sprite sp;
    	
    	public FigureSquare(int x,int y,boolean bomb) {
    		sx = x;
    		sy = y;
    		isBomb = bomb;
    	}
    	
    	public void moveSPos(int x,int y) {
        	this.sx = x;
        	this.sy = y;
        }
    }
}
