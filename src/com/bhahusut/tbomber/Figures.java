package com.bhahusut.tbomber;

import org.andengine.util.color.Color;


class FigureI extends Figure  {

    protected FigureI() {        
        
       Figure.FigureSquare fs = new Figure.FigureSquare(0,0,false);
       fsA.add(fs);       
       fs = new Figure.FigureSquare(0,1,false);
       fsA.add(fs);
       fs = new Figure.FigureSquare(0,2,false);
       fsA.add(fs);
       fs = new Figure.FigureSquare(0,3,false);
       fsA.add(fs);
    }
            
    protected void rotationRight() {        
        
        if (curRotation == 0) {//start state
        	fsA.get(1).moveSPos(1, 0);
        	fsA.get(2).moveSPos(2, 0);
        	fsA.get(3).moveSPos(3, 0);        		        
	        
	        curRotation = 1;
        } else if (curRotation == 1) {
        	fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(0,2);
        	fsA.get(3).moveSPos(0,3);        	        	
	        
	        curRotation = 0;
        }
    }
    
    protected void rotationLeft() {
        rotationRight();
    }
    
    
    protected int getGridVal() {
        return I;
    }
        
}

class FigureT extends Figure {
    
    private int[][] rotations; 
              
    protected FigureT() {        
        Figure.FigureSquare fs = new Figure.FigureSquare(0,0,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(1,0,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,1,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(2,0,false);
        fsA.add(fs);
        
        rotations = new int[8][4];
        rotations[0] = new int[] {0,1,1,2};
        rotations[1] = new int[] {0,0,1,0};
        
        rotations[2] = new int[] {0,1,1,1};
        rotations[3] = new int[] {1,0,1,2};
        
        rotations[4] = new int[] {0,1,1,2};
        rotations[5] = new int[] {1,0,1,1};
        
        rotations[6] = new int[] {0,0,0,1};
        rotations[7] = new int[] {0,1,2,1};
        curRotation = 0;
    }    
    
    protected void rotationRight() {
        if(curRotation == 0) {            
            
            fsA.get(0).moveSPos(1, 0);
            fsA.get(1).moveSPos(1, 1);
        	fsA.get(2).moveSPos(0, 1);
        	fsA.get(3).moveSPos(1, 2);        		        	                                
            
            curRotation = 1;
        } else if(curRotation == 1) {                                                                       
            
            fsA.get(0).moveSPos(2, 1);
            fsA.get(1).moveSPos(1, 1);
        	fsA.get(2).moveSPos(1, 0);
        	fsA.get(3).moveSPos(0,1);                       
        	        	
            curRotation = 2;
        } else if(curRotation == 2) {                        
            
        	fsA.get(0).moveSPos(0,2);
            fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(0,0);                               	                        
            
            curRotation = 3;
        } else if(curRotation == 3) {                                   
            fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(1,0);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(2,0);
                        
            curRotation = 0;
        }
    }
    
    protected void rotationLeft() {
        if(curRotation == 0) {                             
            fsA.get(0).moveSPos(0,2);
            fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(0,0);
                        
            curRotation = 3;
        } else if(curRotation == 3) {                                    
            fsA.get(0).moveSPos(2,1);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(0,1);
                        
            curRotation = 2;
        } else if(curRotation == 2) {                       
            fsA.get(0).moveSPos(1,0);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(0,1);
        	fsA.get(3).moveSPos(1,2);                        
            
            curRotation = 1;
        } else if(curRotation == 1) {                                    
            fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(1,0);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(2,0);                        
                        
            curRotation = 0;
        }
    }
        
    protected int getGridVal() {
        return T;
    }        
}

class FigureO extends Figure {

    protected FigureO() {        
        Figure.FigureSquare fs = new Figure.FigureSquare(0,0,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(0,1,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,0,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,1,false);
        fsA.add(fs);
    }
    
    protected void rotationRight() {}

    protected void rotationLeft() {}
    
    
    protected int getGridVal() {
        return O;
    }        
}

class FigureL extends Figure {
    
    private int[][] rotations; 
        
    protected FigureL() {
    	
    	Figure.FigureSquare fs = new Figure.FigureSquare(0,0,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(0,1,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(0,2,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,2,false);    	                        
        fsA.add(fs);
        
        rotations = new int[8][4];
        rotations[0] = new int[] {0,0,0,1};
        rotations[1] = new int[] {0,1,2,2};
        
        rotations[2] = new int[] {0,0,1,2};
        rotations[3] = new int[] {0,1,0,0};
        
        
        
        rotations[4] = new int[] {0,1,1,1};
        rotations[5] = new int[] {0,0,1,2};
        
        
        
        rotations[6] = new int[] {0,1,2,2};
        rotations[7] = new int[] {1,1,0,1};
        
        
        
        curRotation = 0;
    }
    
    protected void rotationRight() {
        if(curRotation == 0) {
        	fsA.get(0).moveSPos(2,0);
            fsA.get(1).moveSPos(1,0);
        	fsA.get(2).moveSPos(0,0);
        	fsA.get(3).moveSPos(0,1);
        	
            curRotation = 1;
        } else if(curRotation == 1) {
        	fsA.get(0).moveSPos(1,2);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(0,0);
        	
            curRotation = 2;
        } else if(curRotation == 2) {                        
            fsA.get(0).moveSPos(0,1);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(2,1);
        	fsA.get(3).moveSPos(2,0);
            
            curRotation = 3;
        } else if(curRotation == 3) {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(0,2);
        	fsA.get(3).moveSPos(1,2);
        	
            curRotation = 0;
        }
    }
    
    protected void rotationLeft() {
        if(curRotation == 0) {
        	fsA.get(0).moveSPos(0,1);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(2,1);
        	fsA.get(3).moveSPos(2,0);
        	        	        	
            curRotation = 3;
        } else if(curRotation == 3) {
        	fsA.get(0).moveSPos(1,2);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(0,0);
        	
            curRotation = 2;
        } else if(curRotation == 2) {
        	fsA.get(0).moveSPos(2,0);
            fsA.get(1).moveSPos(1,0);
        	fsA.get(2).moveSPos(0,0);
        	fsA.get(3).moveSPos(0,1);
        	
            curRotation = 1;
        } else if(curRotation == 1) {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(0,2);
        	fsA.get(3).moveSPos(1,2);
        	
            curRotation = 0;
        }
    }
    
    
    protected int getGridVal() {
        return L;
    }
       
}

class FigureJ extends Figure {
    
    private int[][] rotations; 

    protected FigureJ() {            
        Figure.FigureSquare fs = new Figure.FigureSquare(0,2,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(1,0,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,1,false);
        fsA.add(fs);        
        fs = new Figure.FigureSquare(1,2,false);
        fsA.add(fs);
        
        rotations = new int[8][4];
        rotations[0] = new int[] {0,1,1,1};
        rotations[1] = new int[] {2,0,1,2};                
        
        rotations[2] = new int[] {0,0,1,2};
        rotations[3] = new int[] {0,1,1,1};
        
        rotations[4] = new int[] {0,0,0,1};
        rotations[5] = new int[] {0,1,2,0};
        
        rotations[6] = new int[] {0,1,2,2};
        rotations[7] = new int[] {0,0,0,1};
        curRotation = 0;
    }

    protected void rotationRight() {
        if(curRotation == 0) {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(2,1);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(0,1);
        	
            curRotation = 1;
        } else if(curRotation == 1) {
        	fsA.get(0).moveSPos(1,0);
            fsA.get(1).moveSPos(0,2);
        	fsA.get(2).moveSPos(0,1);
        	fsA.get(3).moveSPos(0,0);
        	
            curRotation = 2;
        } else if(curRotation == 2) {
        	fsA.get(0).moveSPos(2,1);
            fsA.get(1).moveSPos(0,0);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(2,0);
        	
            curRotation = 3;
        } else if(curRotation == 3) {
        	fsA.get(0).moveSPos(0,2);
            fsA.get(1).moveSPos(1,2);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(1,0);
        	        	
            curRotation = 0;
        }
    }
    
    protected void rotationLeft() {
        if(curRotation == 0) {
        	fsA.get(0).moveSPos(2,1);
            fsA.get(1).moveSPos(2,0);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(0,0);

            curRotation = 3;
        } else if(curRotation == 3) {
        	fsA.get(0).moveSPos(1,0);
            fsA.get(1).moveSPos(0,0);
        	fsA.get(2).moveSPos(0,1);
        	fsA.get(3).moveSPos(0,2);
        	
            curRotation = 2;
        } else if(curRotation == 2) {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(0,1);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(2,1);

            curRotation = 1;
        } else if(curRotation == 1) {
        	fsA.get(0).moveSPos(0,2);
            fsA.get(1).moveSPos(1,2);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(1,0);
        	
            curRotation = 0;
        }
    }

    protected int getGridVal() {
        return J;
    }
        
}

class FigureS extends Figure {
    
    private int[][] rotations; 

    protected FigureS() {                
        Figure.FigureSquare fs = new Figure.FigureSquare(0,1,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(1,0,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,1,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(2,0,false);
        fsA.add(fs);
        
        rotations = new int[4][4];
        rotations[0] = new int[] {0,1,1,2};
        rotations[1] = new int[] {1,0,1,0};
        rotations[2] = new int[] {0,0,1,1};
        rotations[3] = new int[] {0,1,1,2};
        curRotation = 0;
    }

    protected void rotationRight() {
        if(curRotation == 0) {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(0,1);
        	fsA.get(3).moveSPos(1,2);
        	
            curRotation = 1;
            
        } else {
        	fsA.get(0).moveSPos(2,0);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(1,0);
        	fsA.get(3).moveSPos(0,1);
            curRotation = 0;
        }
    }

    protected void rotationLeft() {
        rotationRight();
    }

    protected int getGridVal() {
        return S;
    }
    
   }

class FigureZ extends Figure {

    private int[][] rotations;     
    
    protected FigureZ() {                
        Figure.FigureSquare fs = new Figure.FigureSquare(0,0,false);
        fsA.add(fs);       
        fs = new Figure.FigureSquare(1,0,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(1,1,false);
        fsA.add(fs);
        fs = new Figure.FigureSquare(2,1,false);
        fsA.add(fs);
        
        rotations = new int[4][4];
        rotations[0] = new int[] {0,1,1,2};
        rotations[1] = new int[] {0,0,1,1};
        rotations[2] = new int[] {1,0,1,0};
        rotations[3] = new int[] {0,1,1,2};
        curRotation = 0;
    }
    
    protected void rotationRight() {
        if(curRotation == 0) {        	
        	fsA.get(0).moveSPos(1,0);
            fsA.get(1).moveSPos(1,1);
        	fsA.get(2).moveSPos(0,1);
        	fsA.get(3).moveSPos(0,2);
        	        	        	
            curRotation = 1;
            
        } else {
        	fsA.get(0).moveSPos(0,0);
            fsA.get(1).moveSPos(1,0);
        	fsA.get(2).moveSPos(1,1);
        	fsA.get(3).moveSPos(2,1);
        	
            curRotation = 0;
        }
    }
    
    protected void rotationLeft() {
        rotationRight();
    }
    
    protected int getGridVal() {
        return Z;
    }
        
}
