package com.bhahusut.tbomber;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import com.bhahusut.tbomber.Cage.CageGarbage;
import com.bhahusut.tbomber.Figure.FigureSquare;

import android.util.Log;

public class PlayScene extends BaseScene {
	BitmapTextureAtlas boxT,padT,gboxT,cboxT,cageT,fpadT,explode40T;
	ITextureRegion boxTR,padTR,gboxTR,cboxTR,cageTR,fpadTR;
	Sprite boxSp,padSp,gboxSp,rpadSp,lpadSp,cboxSp,cageSp,fpadSp;
	BitmapTextureAtlas bomb40T,bomb80T,bomb160T,explode80T,explode160T;
	ITextureRegion bomb40TR,bomb80TR,bomb160TR;
	Sprite h_rpad,h_lpad,h_dpad,h_r1pad,h_r2pad,bomb40Sp,bomb80Sp,bomb160Sp;
			
	Sprite touchedSp;
	
	ITiledTextureRegion explode40TTR,explode80TTR,explode160TTR;

	Font scoreLabelF;
	Font scoreF;
	
	
	public Timer touchedTimer;
	TouchedTimerTask touchedTimerTask;
	public static final int touchDelay = 150;
	public static final int gTouchedTime = 600;
	PlayScene sc;	
	Cage cage;			
	public boolean isTouched = false;
	public enum PadTouch {
		left,right,down,rotate_cw,rotate_ccw,none;
	};
	final int qFigureOffsetX=20;
	final int qFigureOffsetY=cage.cageOffsetY+75;
	final int waitBoxW = 130;
	final int waitBoxH = 130;
	final float qScale = 0.5f;
	int scoreOffsetX = 620;
	int scoreOffsetY = 300;
	int timeOffsetX = 600;
	int timeOffsetY = 200;
	Text scoreT,scoreLabelT,timeT;
	int timeSec=0,timeMin=0,timeHour=0;
	
	ArrayList<Figure> qFigure = new ArrayList<Figure>();
	
	PlayScene(Engine _eg, MainActivity _act, VertexBufferObjectManager _vbo,
			Camera _camera) {
		super(_eg, _act, _vbo, _camera);
		// TODO Auto-generated constructor stub
		sc = this;
		this.createScene();
		
	}

	public PadTouch padTouchedXY(int tx,int ty) {
		PadTouch pt=PadTouch.none;
		//left pad button
		if (tx >= 195-50 && tx <=195+92-50 && ty >= 988 && ty <= 988+87) {
			return PadTouch.left;
		//right pad button
		} else if (tx >= 300-50 && tx <=300+92-50 && ty >= 988 && ty <= 988+87) {					 						
			return PadTouch.right;
		//down pad button
		} else if (tx >= 252-50 && tx <=252+92-50 && ty >= 1088 && ty <= 1088+87) { 						
			return PadTouch.down;				
		//right rotate
		} else if (tx >= 399+50 && tx <=399+92+50  && ty >= 988 && ty <= 988+87) {
			return PadTouch.rotate_cw;
		//left rotate
		} else if (tx >= 502+50 && tx <= 502+92+50 && ty >= 988 && ty <= 988+87) {
			return PadTouch.rotate_ccw;
		}
		
		return pt;
	}	
	
	@Override
	public void createScene() {
		// TODO Auto-generated method stub
		Log.e("PlayScene","creating scene...");
						
		this.loadGFX();
		this.loadFont();	
		this.initHighlightPad();		
					
		this.setBackground(new Background(0.82f,0.83f,0.83f,1));
				
		cage = new Cage(sc);				
		cage.drawCage();
		this.drawPad();
		initScore();
		initTime();
		initTimeUpdating();
		
		cage.initBombCoordinate();
		this.createOnTouchPlayScene();
						
		qFigure.add(cage.genNewFigure());
		qFigure.add(cage.genNewFigure());
		qFigure.add(cage.genNewFigure());
		sc.updateQFigure();
		
		cage.newFallingFigure();
			
		//this.createTestBomb();
		//this.createTestSucking();						
		//cage.sumBomb80FromSmallBomb();		
		//cage.sumBomb160FromBomb80();		
		//cage.suckToBottom();		
		//cage.clearFullLineGarbage();
		
		cage.initSmallBombQueueHandler();		
		cage.initBomb80QueueHandler();
		cage.initBomb160QueueHandler();		
		
		cage.startGravity(cage.gTime);										
	}									
	
	public void createTestSucking() {
		cage.addCageGarbage(cage.cageOffsetX,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*19,false);		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*19,false);		
		
		/*
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*8,
				cage.cageOffsetY+cage.boxH*19,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*9,
				cage.cageOffsetY+cage.boxH*19,false);
		*/
		
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*18,false);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*18,false);
						
				
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*8,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*8,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*8,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*8,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*9,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*9,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*9,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*9,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*10,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*10,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*10,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*10,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*11,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*11,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*11,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*11,true);
	}
	
	public void createTestBomb() {										
		//close line			
		
		/*
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*18,true);
		*/
		
		cage.addCageGarbage(cage.cageOffsetX,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*19,true	);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*19,true);
		
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*8,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*9,
				cage.cageOffsetY+cage.boxH*19,true);
		
		
		cage.addCageGarbage(cage.cageOffsetX,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*18,true	);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*18,true);
		
		/*
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*8,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*9,
				cage.cageOffsetY+cage.boxH*18,true);
		*/
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*16,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*16,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*3,
				cage.cageOffsetY+cage.boxH*16,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*2,
				cage.cageOffsetY+cage.boxH*16,true);
	}
	
	public void testCreateBomb160() {		
		//create same 80bomb 4 ea
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*16,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*16,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*17,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*4,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*5,
				cage.cageOffsetY+cage.boxH*19,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*18,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*18,true);				
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*19,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*19,true);
		
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*16,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*16,true);				
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*6,
				cage.cageOffsetY+cage.boxH*17,true);
		cage.addCageGarbage(cage.cageOffsetX+cage.boxW*7,
				cage.cageOffsetY+cage.boxH*17,true);					
		
	}
	
	public void initTimeUpdating() {
		sc.registerUpdateHandler(new TimerHandler(1, 
				true, new ITimerCallback() {
            @Override
            public void onTimePassed(final TimerHandler pTimerHandler) {         	
            	timeSec++;
            	if (timeSec == 60) {
            		timeSec = 0;
            		timeMin++;
            		if (timeMin == 60) {
            			timeMin = 0;
            			timeHour++;
            		}
            	}
            		
            	String secStr = String.format("%02d",timeSec);
            	String minStr = String.format("%02d",timeMin);
            	String hourStr = String.format("%02d",timeHour);
           
            	timeT.setText(hourStr+":"+minStr+":"+secStr);
            	
            }
		}));                       
	}
	
	
	public void drawHighlightPad(PadTouch pt) {				
		if (pt == PadTouch.left) {
			sc.attachChild(h_lpad);
		} else if (pt == PadTouch.right) {
			sc.attachChild(h_rpad);
		} else if (pt == PadTouch.down) {
			sc.attachChild(h_dpad);
		} else if (pt == PadTouch.rotate_cw) {
			sc.attachChild(h_r1pad);
		} else if (pt == PadTouch.rotate_ccw) {
			sc.attachChild(h_r2pad);
		}
	}
	
	public void detachHighlightPad(PadTouch pt) {
		if (pt == PadTouch.left) {
			detachSprite(h_lpad);
		} else if (pt == PadTouch.right) {
			detachSprite(h_rpad);
		} else if (pt == PadTouch.down) {
			detachSprite(h_dpad);
		} else if (pt == PadTouch.rotate_cw) {
			detachSprite(h_r1pad);
		} else if (pt == PadTouch.rotate_ccw) {
			detachSprite(h_r2pad);
		}
	}
	
	public void initScore() {
		scoreLabelT = new Text(scoreOffsetX,scoreOffsetY,
				scoreLabelF, "��ṹ", vbo);							
		attachChild(scoreLabelT);
				
		scoreT = new Text(scoreOffsetX,scoreOffsetY+80,
				scoreF, "0",10, vbo);
		int scoreMarginX = 
				(int)scoreLabelT.getWidth()/2-(int)scoreT.getWidth()/2;
		scoreT.setPosition(scoreOffsetX+scoreMarginX, scoreOffsetY+80);		
		attachChild(scoreT);
	}
	
	public void initTime() {
		timeT = new Text(timeOffsetX,timeOffsetY,
				scoreLabelF, "00:00:00",8, vbo);							
		attachChild(timeT);
				
	}
	
	public void updateScore(int _score) {					
		scoreT.setText(_score+"");
		
		int scoreMarginX = 
				(int)scoreLabelT.getWidth()/2-(int)scoreT.getWidth()/2;
		scoreT.setPosition(scoreOffsetX+scoreMarginX, scoreOffsetY+80);
	}
	
	
	
	public void updateQFigure() {		
		for (int i=0;i<3;i++) {						
			if (!qFigure.get(i).fsA.get(0).sp.hasParent())
				cage.drawScaleFigureSprite(qFigure.get(i), qScale);
								
			int offsetX = 
					(int)(qFigureOffsetX+waitBoxW/2-
						(qFigure.get(i).getMaxRightPos()+1)*qScale
								/2*cage.boxW);
			int offsetY = 
					(int)(qFigureOffsetY+waitBoxH/2-
					(qFigure.get(i).getMaxBottomPos()+1)*qScale/2*cage.boxH
					+i*150);
					
			qFigure.get(i).setOffset(offsetX,offsetY);	
										
		}
	}		

	public void detachFigure(Figure f) {
		for (FigureSquare fs : f.fsA) {
			if (!fs.sp.hasParent())
				detachSprite(fs.sp);
		}
	}
	
	public void createOnTouchPlayScene() {								
		touchedTimer = new Timer();				
		
		// touching				
		this.setOnSceneTouchListener(new IOnSceneTouchListener() {
			@Override
			public boolean onSceneTouchEvent(Scene pScene,
					TouchEvent pSceneTouchEvent) {		
				
				float tx = pSceneTouchEvent.getX();
				float ty = pSceneTouchEvent.getY();				
				
				if (pSceneTouchEvent.isActionDown()) {										
					isTouched = true;
					touchedTimer = new Timer();
					touchedTimerTask = 
							new TouchedTimerTask(tx,ty);											
					touchedTimer.schedule(touchedTimerTask,0,touchDelay);
					
					//draw highlight pad
					//sc.drawHighlightPad(
						//	sc.padTouchedXY((int)tx, (int)ty));
				}
				if (pSceneTouchEvent.isActionUp()) {
					isTouched = false;
					touchedTimer.cancel();
										
					sc.detachHighlightPad(
							sc.padTouchedXY((int)tx, (int)ty));
					
				//	cage.stopGravity();
				//	cage.startGravity(cage.gTime);
									
					if (sc.padTouchedXY((int)tx, (int)ty) == PadTouch.rotate_cw) {
						//right rotate
						Figure f=cage.dup4ChkRotate();																														
						f.rotationRight();		
						cage.genFigureSprite(f);						
												
						if (!cage.isFigureCollidAnyGarbage(f) &&
								!cage.isFigureOutCage(f)) {
							cage.figure.rotationRight();
							cage.genFigureSprite(cage.figure);
							cage.drawFigureSprite(cage.figure);
						}											
					} else if (sc.padTouchedXY((int)tx, (int)ty) == PadTouch.rotate_ccw) { 
						//left rotate										
						Figure f=cage.dup4ChkRotate();																														
						f.rotationLeft();	
						cage.genFigureSprite(f);												
						
						if (!cage.isFigureOutCage(f) &&
								!cage.isFigureCollidAnyGarbage(f)) {													
							cage.figure.rotationLeft();
							cage.genFigureSprite(cage.figure);
							cage.drawFigureSprite(cage.figure);
						}
					}					
				}	
				
				return false;																							
				
			}
		});
	}
	
	@Override
	public void loadGFX() {
		// TODO Auto-generated method stub
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");						
		
		fpadT = new BitmapTextureAtlas(eg.getTextureManager(),
				460, 176, TextureOptions.BILINEAR);						
		fpadTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						fpadT, act, "fpad.png",0,0,5,2);
		fpadT.load();
		
		cageT = new BitmapTextureAtlas(eg.getTextureManager(),
				597, 812, TextureOptions.BILINEAR);						
		cageTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						cageT, act, "cage.png",0,0,1,1);
		cageT.load();		
		
		cboxT = new BitmapTextureAtlas(eg.getTextureManager(),
				160, 80, TextureOptions.BILINEAR);						
		cboxTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						cboxT, act, "cbox.png",0,0,4,2);			
		cboxT.load();		
		
		bomb40T = new BitmapTextureAtlas(eg.getTextureManager(),
				40, 40, TextureOptions.BILINEAR);						
		bomb40TR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						bomb40T, act, "bomb40.png",0,0,1,1);			
		bomb40T.load();
		
		bomb80T = new BitmapTextureAtlas(eg.getTextureManager(),
				80, 80, TextureOptions.BILINEAR);						
		bomb80TR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						bomb80T, act, "bomb80.png",0,0,1,1);			
		bomb80T.load();
		
		bomb160T = new BitmapTextureAtlas(eg.getTextureManager(),
				160, 160, TextureOptions.BILINEAR);						
		bomb160TR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						bomb160T, act, "bomb160.png",0,0,1,1);			
		bomb160T.load();
		
		explode40T = new BitmapTextureAtlas(eg.getTextureManager(),
				994, 120, TextureOptions.BILINEAR);
		explode40TTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						explode40T, act, "explode40.png",0,0,7,1);
		explode40T.load();
		
		explode80T = new BitmapTextureAtlas(eg.getTextureManager(),
				1200, 900, TextureOptions.BILINEAR);
		explode80TTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						explode80T, act, "explode80.png",0,0,4,3);
		explode80T.load();
						
		
		explode160T = new BitmapTextureAtlas(eg.getTextureManager(),
				1855, 2048, TextureOptions.BILINEAR);
		explode160TTR = 
				BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
						explode160T, act, "explode160.png",0,0,4,4);
		explode160T.load();
		
	}
		
	public void drawPad() {								
		fpadTR.setTexturePosition(0,0);
		Sprite lpad = new Sprite(195-50,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92,0);
		Sprite rpad = new Sprite(300-50,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92*2,0);
		Sprite dpad = new Sprite(252-50,1087,fpadTR,vbo);
		fpadTR.setTexturePosition(92*3,0);
		Sprite r1pad = new Sprite(399+50,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92*4,0);
		Sprite r2pad = new Sprite(502+50,988,fpadTR,vbo);
		
		sc.attachChild(lpad);
		sc.attachChild(rpad);
		sc.attachChild(dpad);
		sc.attachChild(r1pad);
		sc.attachChild(r2pad);
		
	}
	
	public void initHighlightPad() {
		fpadTR.setTexturePosition(0,87);
		h_lpad = new Sprite(195,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92,87);
		h_rpad = new Sprite(300,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92*2,87);
		h_dpad = new Sprite(252,1087,fpadTR,vbo);
		fpadTR.setTexturePosition(92*3,87);
		h_r1pad = new Sprite(399,988,fpadTR,vbo);
		fpadTR.setTexturePosition(92*4,87);
		h_r2pad = new Sprite(502,988,fpadTR,vbo);		
	}	
	
	public void loadFont() {
		FontFactory.setAssetBasePath("fonts/");
        final ITexture fontTexture = new BitmapTextureAtlas(
					act.getTextureManager(), 1024, 1024,
					TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        final ITexture fontTexture1 = new BitmapTextureAtlas(
				act.getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		scoreLabelF = FontFactory.createFromAsset(act.getFontManager(), 
				fontTexture,act.getAssets(), "THSarabun Bold.ttf", 60, true,
				Color.BLACK.getARGBPackedInt());	
		scoreLabelF.load();
		
		scoreF = FontFactory.createFromAsset(act.getFontManager(), 
				fontTexture1,act.getAssets(), "TAHOMA.TTF", 30, true,
				Color.PINK.getARGBPackedInt());
		scoreF.load();			
		

	}
	
	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposeScene() {
		// TODO Auto-generated method stub
		
	}			

	public void detachSprite(final Sprite sp) {
		eg.runOnUpdateThread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				sp.detachSelf();
			}						
		});
	}
	
	public class TouchedTimerTask extends TimerTask {
		float tx,ty;
		
		TouchedTimerTask(float _tx,float _ty) {
			tx = _tx;
			ty = _ty;
		}
		
		@Override
		public void run() {
			// TODO Auto-generated method stub											
			//left pad button
			if (sc.padTouchedXY((int)tx, (int)ty) == PadTouch.left) {																																
				if (!cage.isFigureTouchLeftBorder() &&
						!cage.isFigureLeftSideCollidGarbage(cage.figure)) {
					cage.figure.setOffset(cage.figure.offsetX-cage.boxW, 
							cage.figure.offsetY);
				}												
				
			//right pad button
			} else if (sc.padTouchedXY((int)tx, (int)ty) == PadTouch.right) {					 																									
				if (!cage.isFigureTouchRightBorder() &&
						!cage.isFigureRightSideCollidGarbage(cage.figure)) {
					cage.figure.setOffset(cage.figure.offsetX+cage.boxW, 
							cage.figure.offsetY);
				}
			//down pad button
			} else if (sc.padTouchedXY((int)tx, (int)ty) == PadTouch.down) { 						
				cage.stopGravity();
				cage.startGravity(sc.gTouchedTime);				
			}
		}		
	};
	
	
	
}
